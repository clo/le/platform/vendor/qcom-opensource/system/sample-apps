/*
 * Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef WIFI_H
#define WIFI_H

G_BEGIN_DECLS

gboolean is_wifi_on ();
gboolean enable_wifi ();
gboolean disable_wifi ();

G_END_DECLS

#endif // WIFI_H
